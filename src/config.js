export const config = {
  API_URL: process.env.API_URL || 'TD61sl2bAcPHN6geX1Bd7TU0uABiMjvW', // For demonstration purposes is exposed
  RESULTS_BATCH_SIZE: process.env.RESULTS_COUNT || 25,
  LOCAL_STORAGE_KEY: process.env.LOCAL_STORAGE_KEY || 'gifty_local_storage_state'
};
