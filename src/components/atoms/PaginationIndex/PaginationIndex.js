import React from 'react';
import './PaginationIndex.css';

function PaginationIndex(props) {
  return (
      <div id="pagination-index" data-testid="pagination-index">
        {props.offset + props.count}/{props.totalCount}
      </div>
  );
}

export default PaginationIndex;
