import PaginationIndex from './PaginationIndex';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const component = render(<PaginationIndex/>);
  expect(component.baseElement).toBeInTheDocument();
});
