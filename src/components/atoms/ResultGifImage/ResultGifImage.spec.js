import ResultGifImage from './ResultGifImage';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const resultGifImage = render(<ResultGifImage/>);
  expect(resultGifImage.baseElement).toBeInTheDocument();
});
