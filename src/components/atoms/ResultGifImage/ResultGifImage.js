import React from 'react';
import './ResultGifImage.css';

function ResultGifImage(props) {
  return (
      <img alt={props.alt} src={props.src} className="result-gif-image"/>
  );
}

export default ResultGifImage;
