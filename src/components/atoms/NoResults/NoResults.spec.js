import NoResults from './NoResults';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const noResults = render(<NoResults/>);
  expect(noResults.baseElement).toBeInTheDocument();
});
