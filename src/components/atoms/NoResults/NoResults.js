import React from 'react';
import './NoResults.css';

class NoResults extends React.Component {
  render() {
    return (
        <div id="no-results" data-testid="no-results">
          No Results
        </div>
    );
  }
}

export default NoResults;
