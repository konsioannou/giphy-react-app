import React from 'react';
import './ClearButton.css';

class ClearButton extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    this.props.onClearClick();
  }

  render() {
    return (
        <button data-testid="clear-button"
                id="clear-button"
                onClick={this.handleClick}>
          Clear
        </button>
    );
  }
}

export default ClearButton;
