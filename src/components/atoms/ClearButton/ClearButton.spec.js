import ClearButton from './ClearButton';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const clearButton = render(<ClearButton/>);
  expect(clearButton.baseElement).toBeInTheDocument();
});
