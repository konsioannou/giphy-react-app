import PaginationPreviousButton from './PaginationPreviousButton';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const component = render(<PaginationPreviousButton/>);
  expect(component.baseElement).toBeInTheDocument();
});
