import React from 'react';
import './PaginationPreviousButton.css';

class PaginationPreviousButton extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    this.props.onPaginationPreviousClick();
  }

  render() {
    return (
        <button id="pagination-previous-button"
                disabled={this.props.isLoading || this.props.isDisabled}
                onClick={this.handleClick}>Previous</button>
    );
  }
}

export default PaginationPreviousButton;
