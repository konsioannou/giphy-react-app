import PaginationNextButton from './PaginationNextButton';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const component = render(<PaginationNextButton/>);
  expect(component.baseElement).toBeInTheDocument();
});
