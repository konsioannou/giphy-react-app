import React from 'react';
import './PaginationNextButton.css';

class PaginationNextButton extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    this.props.onPaginationNextClick();
  }

  render() {
    return (
        <button data-testid="pagination-next-button"
                disabled={this.props.isLoading || this.props.isDisabled}
                id="pagination-next-button"
                onClick={this.handleClick}>
          Next
        </button>
    );
  }
}

export default PaginationNextButton;
