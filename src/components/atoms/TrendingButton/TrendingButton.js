import React from 'react';
import './TrendingButton.css';

class TrendingButton extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    this.props.onTrendingClick();
  }

  render() {
    return (
        <button data-testid="trending-button"
                id="trending-button"
                onClick={this.handleClick}>
          Trending
        </button>
    );
  }
}

export default TrendingButton;
