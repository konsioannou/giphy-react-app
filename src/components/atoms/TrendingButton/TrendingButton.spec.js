import TrendingButton from './TrendingButton';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const trendingButton = render(<TrendingButton/>);
  expect(trendingButton.baseElement).toBeInTheDocument();
});
