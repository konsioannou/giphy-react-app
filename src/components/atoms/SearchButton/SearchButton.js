import React from 'react';
import './SearchButton.css'

function SearchButton() {
  return (
      <input id="search-button" data-testid="search-button" type="submit" value="Submit" name="Search"/>
  );
}

export default SearchButton;
