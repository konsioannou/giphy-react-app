import SearchButton from './SearchButton';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const searchButton = render(<SearchButton/>);
  expect(searchButton.baseElement).toBeInTheDocument();
});
