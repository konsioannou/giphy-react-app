import Title from './Title';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const title = render(<Title/>);
  expect(title.baseElement).toBeInTheDocument();
});
