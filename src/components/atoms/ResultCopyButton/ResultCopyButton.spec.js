import ResultCopyButton from './ResultCopyButton';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const resultCopyButtonComponent = render(<ResultCopyButton/>);
  expect(resultCopyButtonComponent.baseElement).toBeInTheDocument();
});
