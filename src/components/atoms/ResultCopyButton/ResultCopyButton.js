import React from 'react';
import './ResultCopyButton.css';

class ResultCopyButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {buttonStyleClass: 'normal'};
    this.copyToClipboard = this.copyToClipboard.bind(this);
  }

  copyToClipboard() {
    navigator.clipboard.writeText(this.props.gifUrl).then(() => {
      this.setState({buttonStyleClass: 'copied'});
    }, (err) => {
      console.error('Async: Could not copy text: ', err);
    });
  };

  render() {
    return (
        'queryCommandSupported' in document &&
        document.queryCommandSupported('copy') &&
        <button className={'result-copy-button ' + this.state.buttonStyleClass}
                onClick={this.copyToClipboard}>
          {
            this.state.buttonStyleClass === 'copied' ? 'Copied!' : 'Copy'
          }
        </button>
    );
  }
}

export default ResultCopyButton;
