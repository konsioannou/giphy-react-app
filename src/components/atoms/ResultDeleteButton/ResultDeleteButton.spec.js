import ResultDeleteButton from './ResultDeleteButton';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const resultDeleteButton = render(<ResultDeleteButton/>);
  expect(resultDeleteButton.baseElement).toBeInTheDocument();
});
