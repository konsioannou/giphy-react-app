import React from 'react';
import './ResultDeleteButton.css';

class ResultDeleteButton extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    this.props.onDeleteClick();
  }

  render() {
    return (
        <button className="result-delete-button"
                data-testid="result-delete-button"
                onClick={this.handleClick}>
          Delete
        </button>
    );
  }
}

export default ResultDeleteButton;
