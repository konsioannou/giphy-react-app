import React from 'react';
import './SearchInput.css';

class SearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.handleSearchInputValueChange = this
        .handleSearchInputValueChange
        .bind(this);
  }

  handleSearchInputValueChange(event) {
    this.props.onSearchInputValueChange(event.target.value);
  }

  render() {
    return (
        <input data-testid="search-input"
               type="text"
               id="search-input"
               value={this.props.searchInputValue}
               required
               placeholder="search here..."
               onChange={this.handleSearchInputValueChange}/>
    );
  }
}

export default SearchInput;
