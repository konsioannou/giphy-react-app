import SearchInput from './SearchInput';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const searchInput = render(<SearchInput/>);
  expect(searchInput.baseElement).toBeInTheDocument();
});
