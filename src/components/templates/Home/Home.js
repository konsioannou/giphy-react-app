import React from 'react';
import './Home.css';
import Title from '../../atoms/Title/Title';
import TopBar from '../../organisms/TopBar/TopBar';
import Results from '../../organisms/Results/Results';
import GiphyApi from '../../../services/Api/GiphyApi';
import { config } from '../../../config';
import CacheService from '../../../services/Cache/CacheService';

class Home extends React.Component {
  giphyService;
  localStorageKey = config.LOCAL_STORAGE_KEY;
  gifCollection = [];

  constructor(props) {
    super(props);
    this.giphyService = new GiphyApi();

    this.state = {
      searchInputValue: '',
      gifCollection: [],
      isSearchResults: false,
      isLoading: false,
      pagination: {
        totalCount: 0,
        count: 0,
        offset: 0,
      },
    };

    this.handleSearchClick = this.handleSearchClick.bind(this);
    this.handleTrendingClick = this.handleTrendingClick.bind(this);
    this.handleClearClick = this.handleClearClick.bind(this);
    this.handlePreviousButtonClick = this.handlePreviousButtonClick.bind(this);
    this.handleNextButtonClick = this.handleNextButtonClick.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleSearchInputValueChange = this.handleSearchInputValueChange.bind(
        this);
  }

  initialize() {
    const cachedState = CacheService.retrieve(this.localStorageKey);
    if (cachedState) {
      this.setState(cachedState);
    } else {
      this.getTrendingGifIcons();
    }
  }

  saveState() {
    CacheService.save(this.localStorageKey, this.state);
  }

  render() {
    return (
        <div className="App">
          <Title/>
          <TopBar
              onSearchInputValueChange={this.handleSearchInputValueChange}
              searchInputValue={this.state.searchInputValue}
              onSearchClick={this.handleSearchClick}
              onClearClick={this.handleClearClick}
              onTrendingClick={this.handleTrendingClick}/>
          <Results gifCollection={this.state.gifCollection}
                   isLoading={this.state.isLoading}
                   offset={this.state.pagination.offset}
                   count={this.state.pagination.count}
                   totalCount={this.state.pagination.totalCount}
                   isSearchResults={this.state.isSearchResults}
                   onDeleteClick={this.handleDeleteClick}
                   onPaginationPreviousClick={this.handlePreviousButtonClick}
                   onPaginationNextClick={this.handleNextButtonClick}/>
        </div>
    );
  }

  componentDidMount() {
    this.initialize();
  }

  startLoading() {
    this.setState({isLoading: true}, () => this.saveState());
  }

  finishLoading() {
    this.setState({isLoading: false}, () => this.saveState());
  }

  getTrendingGifIcons() {
    this.startLoading();
    this.giphyService
        .getTrending()
        .then(response => {
          this.finishLoading();
          this.updateResults(response, false);
        })
        .catch(error => console.error(error));
  }

  updateResults(response, isSearchResults) {
    this.setState({
      gifCollection: response.data,
      isSearchResults: isSearchResults,
      pagination: {
        totalCount: response.pagination.total_count,
        count: response.pagination.count,
        offset: response.pagination.offset,
      },
    }, () => this.saveState());
  }

  handleSearchClick() {
    const searchTerm = this.state.searchInputValue;
    if (searchTerm) {
      this.searchGifIconsFor(searchTerm);
    } else {
      this.getTrendingGifIcons();
    }
  }

  handleClearClick() {
    this.clearSearchInput();
    this.getTrendingGifIcons();
  }

  searchGifIconsFor(searchTerm) {
    this.startLoading();
    const offset = this.state.pagination.offset;
    this.giphyService
        .search(searchTerm, offset)
        .then(response => {
          this.finishLoading();
          this.updateResults(response, true);
        })
        .catch(error => console.error(error));
  }

  handleTrendingClick() {
    this.clearSearchInput();
    this.getTrendingGifIcons();
  }

  handleDeleteClick(gifId) {
    const currentGifCollection = [...this.state.gifCollection];

    const index = currentGifCollection
        .findIndex((gifItem) => gifItem.id === gifId);

    if (index >= 0) {
      currentGifCollection.splice(index, 1);
      this.setState({gifCollection: currentGifCollection},
          () => this.saveState());
    }
  }

  clearSearchInput() {
    this.setState({searchInputValue: ''}, () => this.saveState());
  }

  handleSearchInputValueChange(searchTerm) {
    this.setState({searchInputValue: searchTerm}, () => this.saveState());
  }

  handleNextButtonClick() {
    const newOffset = this.state.pagination.offset +
        this.state.pagination.count;

    // Nested state property. Better readability. Redux is too much for now ?
    const {pagination} = {...this.state};
    pagination.offset = newOffset;
    this.setState({pagination}, () => this.saveState());

    this.searchGifIconsFor(this.state.searchInputValue);
  }

  handlePreviousButtonClick() {
    let newOffset = this.state.pagination.offset -
        this.state.pagination.count;

    // I know this is ugly.
    // Problem: you are in the last results page and the remaining gifs are e.g 3,
    // the previous button usually goes back by offset - count (the count usually is the page size).
    // But the count now is 3 not equal to the page size.
    // RESULTS_BATCH_SIZE acts as a fixed count.
    // No good explanation either :/
    if ((this.state.pagination.offset + this.state.pagination.count) ===
        this.state.pagination.totalCount) {
      newOffset = this.state.pagination.totalCount -
          (config.RESULTS_BATCH_SIZE + this.state.pagination.count);
    }

    // Nested state property. Better readability. Redux is too much for now ?
    const {pagination} = {...this.state};
    pagination.offset = newOffset;
    this.setState({pagination}, () => this.saveState());

    this.searchGifIconsFor(this.state.searchInputValue);
  }
}

export default Home;
