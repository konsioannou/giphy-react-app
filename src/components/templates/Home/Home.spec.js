import Home from './Home';
import React from 'react';
import { render } from '@testing-library/react';
import TestingUtils from './TestingUtils';
import MockGiphyApi from '../../../services/Api/MockGiphyApi';
import CacheService from '../../../services/Cache/CacheService';

jest.mock('../../../services/Api/GiphyApi');

afterEach(() => {
  CacheService.clear();
});

it('renders without crashing', () => {
  MockGiphyApi.setMockResponses(
      {
        search: {totalCount: 9, count: 9},
        trending: {totalCount: 9, count: 9},
      },
  );
  const homeComponent = render(<Home/>);
  expect(homeComponent.baseElement).toBeInTheDocument();
});

it('should load trending gif icons on start', async () => {
  MockGiphyApi.setMockResponses(
      {
        search: {totalCount: 1, count: 1},
        trending: {totalCount: 5, count: 5},
      },
  );
  const {findAllByTestId} = render(<Home/>);
  const results = await findAllByTestId('result-gif');
  expect(results.length).toBe(5);
});

it('should search for a gif when user types a term in search input and clicks search button',
    async () => {
      MockGiphyApi.setMockResponses(
          {
            search: {totalCount: 20, count: 10},
            trending: {totalCount: 1, count: 1},
          },
      );
      const {getByTestId, findAllByTestId} = render(<Home/>);
      const searchInput = getByTestId('search-input');
      const searchButton = getByTestId('search-button');
      const mockSearchTerm = 'Cat';

      TestingUtils.writeOnInput(searchInput, mockSearchTerm);

      TestingUtils.clickOn(searchButton);

      const results = await findAllByTestId('result-gif');

      expect(searchInput.value).toMatch(mockSearchTerm);
      expect(results.length).toBe(10);
    });

it('should display the trending gif icons when trending button is clicked',
    async () => {
      MockGiphyApi.setMockResponses(
          {
            search: {totalCount: 50, count: 15},
            trending: {totalCount: 20, count: 20},
          },
      );
      const {getByTestId, findAllByTestId} = render(<Home/>);
      const searchInput = getByTestId('search-input');
      const searchButton = getByTestId('search-button');
      const trendingButton = getByTestId('trending-button');

      TestingUtils.writeOnInput(searchInput, 'nba');

      TestingUtils.clickOn(searchButton);

      TestingUtils.clickOn(trendingButton);

      const results = await findAllByTestId('result-gif');

      expect(results.length).toBe(20);
      expect(searchInput.value).toMatch('');
    });

it('should clear the search results and default to trending gif icons',
    async () => {
      MockGiphyApi.setMockResponses(
          {
            search: {totalCount: 1, count: 1},
            trending: {totalCount: 7, count: 7},
          },
      );
      const {getByTestId, findAllByTestId} = render(<Home/>);
      const searchInput = getByTestId('search-input');
      const searchButton = getByTestId('search-button');
      const clearButton = getByTestId('clear-button');

      TestingUtils.writeOnInput(searchInput, 'test');

      TestingUtils.clickOn(searchButton);

      TestingUtils.clickOn(clearButton);

      const results = await findAllByTestId('result-gif');

      expect(results.length).toBe(7);
      expect(searchInput.value).toMatch('');
    });

it('should inform the user when there are no results', () => {
  MockGiphyApi.setMockResponses(
      {
        search: {totalCount: 0, count: 0},
        trending: {totalCount: 3, count: 3},
      },
  );
  const {getByTestId} = render(<Home/>);
  const searchInput = getByTestId('search-input');
  const searchButton = getByTestId('search-button');

  TestingUtils.writeOnInput(searchInput, 'Asdfaasdfa');

  TestingUtils.clickOn(searchButton);

  const noResultsComponent = getByTestId('no-results');
  expect(noResultsComponent).toHaveTextContent('No Results');
});

it('should paginate the gif results', async () => {
  MockGiphyApi.setMockResponses(
      {
        search: {totalCount: 30, count: 25},
        trending: {totalCount: 20, count: 20},
      },
  );
  const {getByTestId, findAllByTestId} = render(<Home/>);
  const searchInput = getByTestId('search-input');
  const searchButton = getByTestId('search-button');
  const mockSearchTerm = 'Paginate!';

  TestingUtils.writeOnInput(searchInput, mockSearchTerm);

  TestingUtils.clickOn(searchButton);

  const results = await findAllByTestId('result-gif');

  expect(searchInput.value).toMatch(mockSearchTerm);
  expect(results.length).toBe(25);
});

it('should load the next batch of results when clicking next', async () => {
  MockGiphyApi.setMockResponses(
      {
        search: {totalCount: 50, count: 20},
        trending: {totalCount: 20, count: 20},
      },
  );
  const {getByTestId, findAllByTestId} = render(<Home/>);

  const searchInput = getByTestId('search-input');
  const searchButton = getByTestId('search-button');
  const mockSearchTerm = 'Paginate again';

  TestingUtils.writeOnInput(searchInput, mockSearchTerm);
  TestingUtils.clickOn(searchButton);

  await findAllByTestId('result-gif');

  const nextButton = getByTestId('pagination-next-button');
  const paginationIndex = getByTestId('pagination-index');

  expect(paginationIndex).toHaveTextContent('20/50');

  TestingUtils.clickOn(nextButton);

  expect(paginationIndex).toHaveTextContent('40/50');
});

it('should delete a gif from the list', async () => {
  MockGiphyApi.setMockResponses(
      {
        search: {totalCount: 0, count: 0},
        trending: {totalCount: 20, count: 20},
      },
  );
  const {getByTestId, findAllByTestId} = render(<Home/>);

  const results = await findAllByTestId('result-gif');

  const resultGif = results[0];

  TestingUtils.hover(resultGif);

  const deleteButton = getByTestId('result-delete-button');

  TestingUtils.clickOn(deleteButton);

  const updatedResults = await findAllByTestId('result-gif');

  expect(updatedResults.length).toBe(19);
});

it('should cache the search results and search input', async () => {
  MockGiphyApi.setMockResponses(
      {
        search: {totalCount: 31, count: 13},
        trending: {totalCount: 20, count: 20},
      },
  );

  const {getByTestId, findAllByTestId, rerender} = render(<Home/>);
  const searchInput = getByTestId('search-input');
  const searchButton = getByTestId('search-button');
  const mockSearchTerm = 'cache';

  TestingUtils.writeOnInput(searchInput, mockSearchTerm);

  TestingUtils.clickOn(searchButton);

  const results = await findAllByTestId('result-gif');

  expect(searchInput.value).toMatch(mockSearchTerm);
  expect(results.length).toBe(13);

  rerender(<Home/>);

  const resultsAfterRefresh = await findAllByTestId('result-gif');
  const searchInputAfterRefresh = getByTestId('search-input');

  expect(searchInputAfterRefresh.value).toMatch(mockSearchTerm);
  expect(resultsAfterRefresh.length).toBe(13);

});
