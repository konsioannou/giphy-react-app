import { fireEvent } from '@testing-library/react';

export default class TestingUtils {
  static writeOnInput(input, text) {
    fireEvent.change(input, {target: {value: text}});
  }

  static clickOn(button) {
    fireEvent.click(button);
  }

  static hover(element) {
    fireEvent.mouseEnter(element);
  }
}
