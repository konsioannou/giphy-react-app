import Pagination from './Pagination';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const component = render(<Pagination/>);
  expect(component.baseElement).toBeInTheDocument();
});

it('should renders the index', () => {
  const {getByTestId} = render(<Pagination offset="0" count="10" totalCount="50"/>);
  const paginationIndex = getByTestId('pagination-index');
  expect(paginationIndex).toHaveTextContent('10/50');
});
