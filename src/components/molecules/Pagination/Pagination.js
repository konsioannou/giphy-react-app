import React from 'react';
import PaginationPreviousButton
  from '../../atoms/PaginationPreviousButton/PaginationPreviousButton';
import PaginationNextButton
  from '../../atoms/PaginationNextButton/PaginationNextButton';
import PaginationIndex from '../../atoms/PaginationIndex/PaginationIndex';
import './Pagination.css';

function Pagination(props) {
  return (
      <section id="pagination">
        <PaginationPreviousButton
            isDisabled={!props.offset}
            isLoading={props.isLoading}
            onPaginationPreviousClick={props.onPaginationPreviousClick}/>
        <PaginationIndex offset={props.offset} count={props.count} totalCount={props.totalCount}/>
        <PaginationNextButton
            isDisabled={(props.offset + props.count) === props.totalCount}
            isLoading={props.isLoading}
            onPaginationNextClick={props.onPaginationNextClick}/>
      </section>
  );
}

export default Pagination;
