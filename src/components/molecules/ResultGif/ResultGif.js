import React from 'react';
import ResultGifImage from '../../atoms/ResultGifImage/ResultGifImage';
import ResultCopyButton from '../../atoms/ResultCopyButton/ResultCopyButton';
import ResultDeleteButton
  from '../../atoms/ResultDeleteButton/ResultDeleteButton';
import './ResultGif.css';

class ResultGif extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHovering: false,
    };

    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
  }

  render() {
    return (
        <div data-testid="result-gif"
             className="result-gif-wrapper"
             onMouseEnter={this.handleMouseEnter}
             onMouseLeave={this.handleMouseLeave}>
          <ResultGifImage alt={this.props.gif.title}
                          src={this.props.gif.images.fixed_width_downsampled.url}/>
          {
            this.state.isHovering && <div className="action-buttons">
              <ResultCopyButton gifUrl={this.props.gif.embed_url}/>
              <ResultDeleteButton onDeleteClick={
                () => this.props.onDeleteClick(this.props.gif.id)
              }/>
            </div>
          }
        </div>
    );
  }

  handleMouseEnter() {
    this.setState({isHovering: true});
  }

  handleMouseLeave() {
    this.setState({isHovering: false});
  }
}

export default ResultGif;
