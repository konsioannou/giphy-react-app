import ResultGif from './ResultGif';
import React from 'react';
import { render } from '@testing-library/react';
import FakeGifFactory from '../../../services/Api/FakeGifFactory';

it('renders without crashing', () => {
  const fakeGif = FakeGifFactory.createFakeGif();
  const resultGif = render(<ResultGif gif={fakeGif}/>);
  expect(resultGif.baseElement).toBeInTheDocument();
});
