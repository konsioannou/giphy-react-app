import React from 'react';
import SearchInput from '../../atoms/SearchInput/SearchInput';
import SearchButton from '../../atoms/SearchButton/SearchButton';

class SearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.onSearchClick();
  }

  render() {
    return (
        <form onSubmit={this.handleSubmit}>
          <SearchInput
              onSearchInputValueChange={this.props.onSearchInputValueChange}
              searchInputValue={this.props.searchInputValue}/>
          <SearchButton/>
        </form>
    );
  }
}

export default SearchForm;
