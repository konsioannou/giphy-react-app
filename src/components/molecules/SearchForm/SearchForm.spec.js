import SearchForm from "./SearchForm";
import React from "react";
import {render} from '@testing-library/react';

it('renders without crashing', () => {
    const searchForm = render(<SearchForm/>);
    expect(searchForm.baseElement).toBeInTheDocument();
});
