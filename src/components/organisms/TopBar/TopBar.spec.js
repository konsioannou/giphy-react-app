import TopBar from './TopBar';
import React from 'react';
import { render } from '@testing-library/react';

it('renders without crashing', () => {
  const topBar = render(<TopBar/>);
  expect(topBar.baseElement).toBeInTheDocument();
});
