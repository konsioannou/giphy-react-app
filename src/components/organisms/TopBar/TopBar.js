import React from 'react';
import SearchForm from '../../molecules/SearchForm/SearchForm';
import TrendingButton from '../../atoms/TrendingButton/TrendingButton';
import './TopBar.css';
import ClearButton from '../../atoms/ClearButton/ClearButton';

function TopBar(props) {
  return (
      <section className="top-bar-wrapper">
        <SearchForm
            onSearchInputValueChange={props.onSearchInputValueChange}
            searchInputValue={props.searchInputValue}
            onSearchClick={props.onSearchClick}/>
        <ClearButton onClearClick={props.onClearClick}/>
        <TrendingButton onTrendingClick={props.onTrendingClick}/>
      </section>
  );
}

export default TopBar;
