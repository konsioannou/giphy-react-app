import Results from "./Results";
import React from "react";
import ReactDOM from 'react-dom';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Results gifCollection={[]}/>, div);
});
