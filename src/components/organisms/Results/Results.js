import React from 'react';
import ResultGif from '../../molecules/ResultGif/ResultGif';
import './Results.css';
import NoResults from '../../atoms/NoResults/NoResults';
import Pagination from '../../molecules/Pagination/Pagination';

class Results extends React.Component {
  render() {
    let resolvedPaginationComponent;

    if (this.props.isSearchResults) {
      resolvedPaginationComponent = <Pagination
          isLoading={this.props.isLoading}
          offset={this.props.offset}
          count={this.props.count}
          totalCount={this.props.totalCount}
          onPaginationPreviousClick={this.props.onPaginationPreviousClick}
          onPaginationNextClick={this.props.onPaginationNextClick}/>;
    }

    const resolvedResultsComponent = this.props.gifCollection.length
        ? this.props.gifCollection.map(
            (gif) => <ResultGif key={gif.id} gif={gif}
                                onDeleteClick={this.props.onDeleteClick}/>)
        : <NoResults/>;

    return (
        <div id="results">
          <section data-testid="result-list" id="result-list">
            {resolvedResultsComponent}
          </section>
          {resolvedPaginationComponent}
        </div>
    );
  }
}

export default Results;
