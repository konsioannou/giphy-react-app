import * as uuid from 'uuid';

export default class FakeGifFactory {
  static createFakeGif() {
    return {
      'type': 'gif',
      'id': uuid.v4(),
      'url': 'https://giphy.com/gifs/nba-f75Hnna9XLYlLaECQZ',
      'slug': 'nba-f75Hnna9XLYlLaECQZ',
      'bitly_gif_url': 'https://gph.is/g/ZYlPOrD',
      'bitly_url': 'https://gph.is/g/ZYlPOrD',
      'embed_url': 'https://giphy.com/embed/f75Hnna9XLYlLaECQZ',
      'username': 'nba',
      'source': 'https://www.nba.com',
      'title': 'Devin Booker GIF by NBA',
      'rating': 'g',
      'content_url': '',
      'source_tld': 'www.nba.com',
      'source_post_url': 'https://www.nba.com',
      'is_sticker': 0,
      'import_datetime': '2020-02-16 02:18:01',
      'trending_datetime': '2020-02-16 21:46:32',
      'images': {
        'downsized_large': {
          'height': '169',
          'size': '784895',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy.gif',
          'width': '300',
        },
        'fixed_height_small_still': {
          'height': '100',
          'size': '7040',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/100_s.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=100_s.gif',
          'width': '178',
        },
        'original': {
          'frames': '46',
          'hash': '49315b39904eedfd977db8939bfaee1a',
          'height': '169',
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy.mp4',
          'mp4_size': '275856',
          'size': '784895',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy.gif',
          'webp': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy.webp?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy.webp',
          'webp_size': '201332',
          'width': '300',
        },
        'fixed_height_downsampled': {
          'height': '200',
          'size': '106500',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200_d.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200_d.gif',
          'webp': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200_d.webp?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200_d.webp',
          'webp_size': '60534',
          'width': '355',
        },
        'downsized_still': {
          'height': '169',
          'size': '30830',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy_s.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy_s.gif',
          'width': '300',
        },
        'fixed_height_still': {
          'height': '200',
          'size': '20044',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200_s.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200_s.gif',
          'width': '355',
        },
        'downsized_medium': {
          'height': '169',
          'size': '784895',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy.gif',
          'width': '300',
        },
        'downsized': {
          'height': '169',
          'size': '784895',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy.gif',
          'width': '300',
        },
        'preview_webp': {
          'height': '169',
          'size': '42250',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy-preview.webp?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy-preview.webp',
          'width': '300',
        },
        'original_mp4': {
          'height': '270',
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy.mp4',
          'mp4_size': '275856',
          'width': '480',
        },
        'fixed_height_small': {
          'height': '100',
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/100.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=100.mp4',
          'mp4_size': '64153',
          'size': '254203',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/100.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=100.gif',
          'webp': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/100.webp?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=100.webp',
          'webp_size': '109816',
          'width': '178',
        },
        'fixed_height': {
          'height': '200',
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200.mp4',
          'mp4_size': '166133',
          'size': '752031',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200.gif',
          'webp': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200.webp?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200.webp',
          'webp_size': '242880',
          'width': '355',
        },
        'downsized_small': {
          'height': '168',
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy-downsized-small.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy-downsized-small.mp4',
          'mp4_size': '140478',
          'width': '300',
        },
        'preview': {
          'height': '134',
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy-preview.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy-preview.mp4',
          'mp4_size': '40546',
          'width': '239',
        },
        'fixed_width_downsampled': {
          'height': '113',
          'size': '44366',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200w_d.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200w_d.gif',
          'webp': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200w_d.webp?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200w_d.webp',
          'webp_size': '25922',
          'width': '200',
        },
        'fixed_width_small_still': {
          'height': '57',
          'size': '3272',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/100w_s.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=100w_s.gif',
          'width': '100',
        },
        'fixed_width_small': {
          'height': '57',
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/100w.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=100w.mp4',
          'mp4_size': '31335',
          'size': '112074',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/100w.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=100w.gif',
          'webp': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/100w.webp?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=100w.webp',
          'webp_size': '57088',
          'width': '100',
        },
        'original_still': {
          'height': '169',
          'size': '30830',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy_s.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy_s.gif',
          'width': '300',
        },
        'fixed_width_still': {
          'height': '113',
          'size': '9851',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200w_s.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200w_s.gif',
          'width': '200',
        },
        'looping': {
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy-loop.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy-loop.mp4',
          'mp4_size': '1186204',
        },
        'fixed_width': {
          'height': '113',
          'mp4': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200w.mp4?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200w.mp4',
          'mp4_size': '69575',
          'size': '331359',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200w.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200w.gif',
          'webp': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/200w.webp?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=200w.webp',
          'webp_size': '119898',
          'width': '200',
        },
        'preview_gif': {
          'height': '51',
          'size': '48514',
          'url': 'https://media3.giphy.com/media/f75Hnna9XLYlLaECQZ/giphy-preview.gif?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=giphy-preview.gif',
          'width': '91',
        },
        '480w_still': {
          'url': 'https://media2.giphy.com/media/f75Hnna9XLYlLaECQZ/480w_s.jpg?cid=dc44ea0d47a10b4f20185dcbfb87570f8e88e57377981d90&rid=480w_s.jpg',
          'width': '480',
          'height': '270',
        },
      },
      'user': {
        'avatar_url': 'https://media0.giphy.com/avatars/nba/GPeEGlo2uy2Z.jpg',
        'banner_url': 'https://media0.giphy.com/channel_assets/nba/NFir0YzR4xTg.GIF',
        'banner_image': 'https://media0.giphy.com/channel_assets/nba/NFir0YzR4xTg.GIF',
        'profile_url': 'https://giphy.com/nba/',
        'username': 'nba',
        'display_name': 'NBA',
        'is_verified': true,
      },
      'analytics': {
        'onload': {
          'url': 'https://giphy-analytics.giphy.com/simple_analytics?response_id=47a10b4f20185dcbfb87570f8e88e57377981d90&event_type=GIF_TRENDING&gif_id=f75Hnna9XLYlLaECQZ&action_type=SEEN',
        },
        'onclick': {
          'url': 'https://giphy-analytics.giphy.com/simple_analytics?response_id=47a10b4f20185dcbfb87570f8e88e57377981d90&event_type=GIF_TRENDING&gif_id=f75Hnna9XLYlLaECQZ&action_type=CLICK',
        },
        'onsent': {
          'url': 'https://giphy-analytics.giphy.com/simple_analytics?response_id=47a10b4f20185dcbfb87570f8e88e57377981d90&event_type=GIF_TRENDING&gif_id=f75Hnna9XLYlLaECQZ&action_type=SENT',
        },
      },
    };
  }

  static create(number) {
    const fakeGifArray = [];
    for (let index = 0; index < number; index++) {
      fakeGifArray.push(this.createFakeGif());
    }
    return fakeGifArray;
  }
}
