import GiphyApi from './GiphyApi';
import FakeGifFactory from './FakeGifFactory';

export default class MockGiphyApi {
  static buildMockResponse(totalCount, count) {
    const mockResponse = {0: []};

    const mockGifArray = FakeGifFactory.create(totalCount);

    for (let offset = 0, total = mockGifArray.length; offset <
    total; offset += count) {
      mockResponse[offset] = mockGifArray.slice(offset, offset + count);
    }
    return mockResponse;
  }

  static setMockResponses({
    search: {totalCount: searchResponseTotalCount, count: searchResponseCount},
    trending: {totalCount: trendingResponseTotalCount, count: trendingResponseCount},
  }) {
    const searchResponse = MockGiphyApi.buildMockResponse(
        searchResponseTotalCount, searchResponseCount);
    const trendingResponse = MockGiphyApi.buildMockResponse(
        trendingResponseTotalCount, trendingResponseCount);

    return GiphyApi.mockImplementation(() => {
      return {
        getTrending: () => Promise.resolve({
          data: trendingResponse[0],
          pagination: {
            total_count: trendingResponseTotalCount,
            count: trendingResponseCount,
            offset: 0,
          },
        }),
        search: (searchTerm, offset) => Promise.resolve({
          data: searchResponse[offset],
          pagination: {
            total_count: searchResponseTotalCount,
            count: searchResponseCount,
            offset,
          },
        }),
      };
    });
  }
}
