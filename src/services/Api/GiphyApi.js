import { config } from '../../config'

export default class GiphyApi {
    search(term, offset) {
        return this.get(`https://api.giphy.com/v1/gifs/search?api_key=${config.API_URL}&q=${term}&limit=25&offset=${offset}&rating=G&lang=en`);
    }

    getTrending() {
        return this.get(`https://api.giphy.com/v1/gifs/trending?api_key=${config.API_URL}&limit=25&rating=G`);
    }

    get(URL) {
        return fetch(URL)
            .then((response) => {
                return response.json();
            });
    }
}
