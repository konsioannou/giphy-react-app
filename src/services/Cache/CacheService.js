export default class CacheService {
  static save(key, data) {
    if (!data) {
      return;
    }
    const serializedData = JSON.stringify(data);
    localStorage.setItem(key, serializedData);
  }

  static retrieve(key) {
    const cachedItem = localStorage.getItem(key);
    if (cachedItem) {
      return JSON.parse(cachedItem);
    }
    return null;
  }

  static clear() {
    localStorage.clear();
  }
}
